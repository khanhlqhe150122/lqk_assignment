package com.example.assignment02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import assignment02.R

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
    }
}