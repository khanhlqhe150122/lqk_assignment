package com.example.assignment02

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import assignment02.R

class SignUpActivity : AppCompatActivity() {

    lateinit var userHm: HashMap<String, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        initAction()
    }

    private fun initAction() {
        userHm = intent.getSerializableExtra("userHm") as HashMap<String, String>
        findViewById<Button>(R.id.btnLogin).setOnClickListener {
            finish()
        }
        findViewById<Button>(R.id.btnSignUp).setOnClickListener {
            signUp()
        }
    }

    private fun signUp() {
        val username = findViewById<TextView>(R.id.editUsername).text.toString()
        val password = findViewById<TextView>(R.id.editPassword).text.toString()
        val reenter = findViewById<TextView>(R.id.editReenter).text.toString()

        when {
            username.isBlank() || password.isBlank() || reenter.isBlank() -> {
                Toast.makeText(this, "Invalid", Toast.LENGTH_SHORT).show()
            }

            userHm.containsKey(username) -> {
                Toast.makeText(this, "Username already exists", Toast.LENGTH_SHORT).show()
            }

            password != reenter -> {
                Toast.makeText(this, "Password re-enter doesn't match", Toast.LENGTH_SHORT).show()
            }

            else -> {
                userHm.put(username, password)
                Toast.makeText(this, "Register successful", Toast.LENGTH_SHORT)

                val intent = Intent()
                intent.putExtra("newUser", username to password)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }

    }
}