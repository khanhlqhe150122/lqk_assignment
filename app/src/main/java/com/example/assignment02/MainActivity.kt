package com.example.assignment02

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import assignment02.R

class MainActivity : AppCompatActivity() {

    var userHm = HashMap<String, String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i("Khanhlq","onCreate: ")

        userHm.put("abc", "123")
        initAction()
    }

    private fun initAction() {
//        val newUser = intent.extras?.get("newUser")
//        if (newUser != null) {
//            userHm += newUser as Pair<String, String>
//        }
        findViewById<Button>(R.id.btnLogin).setOnClickListener {
            checkLogin()
        }
        findViewById<Button>(R.id.btnClear).setOnClickListener {
            clearData()
        }
        findViewById<Button>(R.id.btnSignUp).setOnClickListener {
            startSignUpActivity()
        }
    }

    private fun clearData() {
        findViewById<EditText>(R.id.edtUsername).text.clear()
        findViewById<EditText>(R.id.edtPassword).text.clear()
    }

    private fun startSecondActivity() {
        val intent = Intent(this, SecondActivity::class.java)
        startActivity(intent)
    }

    private fun startSignUpActivity() {
        val intent = Intent(this, SignUpActivity::class.java)
        intent.putExtra("userHm", userHm)
//        val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
//            result: ActivityResult ->
//            if (result.resultCode == Activity.RESULT_OK){
//                val newUser = intent.extras?.get("newUser")
//                userHm += newUser as Pair<String, String>
//            }
//        }
//        startForResult.launch(Intent(this, SignUpActivity::class.java ))
        startActivityForResult(intent, 9)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data:Intent?){
        if (data == null) return
        if(requestCode == 9 && resultCode == Activity.RESULT_OK){
            val newUser = data.extras?.get("newUser")
            userHm += newUser as Pair<String, String>
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun checkLogin() {
        val username = findViewById<EditText>(R.id.edtUsername).text.toString()
        val password = findViewById<EditText>(R.id.edtPassword).text.toString()

        when {
            username.isBlank() || password.isBlank() -> {
                Toast.makeText(this, "Invalid", Toast.LENGTH_SHORT).show()
            }

            userHm[username] == password -> {
//                Toast.makeText(this, "Login success", Toast.LENGTH_SHORT).show()
                startSecondActivity()
            }

            else -> {
                Toast.makeText(this, "Login fail", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onStart(){
        super.onStart()
        Log.i("Khanhlq","onStart: ")
    }

    override fun onRestart(){
        super.onRestart()
        Log.i("Khanhlq","onRestart: ")
    }

    override fun onResume(){
        super.onResume()
        Log.i("Khanhlq","onResume: ")
    }
    
    override fun onPause(){
        super.onPause()
        Log.i("Khanhlq","onPause: ")
    }

    override fun onStop(){
        super.onStop()
        Log.i("Khanhlq","onStop: ")
    }

    override fun onDestroy(){
        super.onDestroy()
        Log.i("Khanhlq","onDestroy: ")
    }
}